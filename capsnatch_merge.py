#!/usr/bin/env python3

from argparse import ArgumentParser
from argparse import RawTextHelpFormatter
from Bio.Seq import Seq
import sys
import os
import re
import pysam
import subprocess
from collections import Counter

''' Prepare data
# Remove adapters and poor quality bases
for i in *_1.fq.gz ;do echo "cutadapt --quality-base=64 -a AGATCGGAAGAGC -A AGATCGGAAGAGC -f fastq -m 50 -q 30 -o ${i/_1.fq.gz/_trimmed_1.fastq.gz} -p ${i/_1.fq.gz/_trimmed_2.fastq.gz} ${i} ${i/_1.fq.gz/_2.fq.gz}";done | parallel -j 6 :::
# replace illigal phred character as vsearch doesn't quite like it
for i in *trimmed_?.fastq.gz;do echo "gunzip -c ${i} | sed 's/j/i/g' | gzip -c > ${i/.fastq.gz/_fixed.fastq.gz}";done | parallel -j4 :::
# merge overlapping reads and convert to ascii 64 phred
for i in *trimmed_1_fixed.fastq.gz ;do echo "vsearch --fastq_mergepairs ${i} --reverse ${i/_1_fixed.fastq.gz/_2_fixed.fastq.gz} --fastqout ${i/_1_fixed.fastq.gz/_merged.fastq} --fastqout_notmerged_fwd ${i/_1_fixed.fastq.gz/_unmerged_fwd.fastq} --fastqout_notmerged_rev ${i/_1_fixed.fastq.gz/_unmerged_rev.fastq} --fastq_ascii 64 --threads 2";done | parallel -j 6 ::: 
# map both read sets
for i in *_merged.fastq;do echo "bwa mem -t 2 -M harm_test/TSWV-lettuce.fasta $i | samtools sort -@2 -O bam -T ${i}_tmp -o ${i/.fastq/_sorted.bam} -"; done | parallel -j3 :::
for i in *_fwd.fastq;do echo "bwa mem -t 2 -M harm_test/TSWV-lettuce.fasta $i ${i/_fwd.fastq/_rev.fastq} | samtools sort -@2 -O bam -T ${i}_tmp -o ${i/_fwd.fastq/_sorted.bam} -"; done | parallel -j6 :::
# index bam files
for i in *trimmed*bam; do echo "samtools index ${i}";done | parallel -j 6 :::
# only retain mapped reads
for i in *trimmed*sorted.bam; do echo "samtools view ${i} -F 4 -b -o ${i/.bam/mapped.bam}";done | parallel -j 6 :::
# index filtered bam files
for i in *sortedmapped.bam; do samtools index $i;done
# merge bamfiles from overlapping and non-overlapping read alignments
for i in *_unmerged_sortedmapped.bam; do echo "samtools merge -O bam ${i%%_*}_trimmed_sorted_mapped_both.bam ${i} ${i/unmerged/merged}";done | parallel -j6 :::
# index final bam
for i in *_trimmed_sorted_mapped_both.bam; do echo "samtools index ${i}";done | parallel -j6 :::
# use these bam files as input for capsnatch.py
'''


# constants
# maximum offset of alignment from end of reference
BAM_MAX_ALIGNMENT_START = 1001
# minimum number of bp clipped from read end (10)
BAM_MIN_LENGTH_SOFTCLIP = 10
# maximum number of bp clipped from read end (100)
BAM_MAX_LENGTH_SOFTCLIP = 100
# maximum percentage AT in clipped read segment
MAX_PERC_AT = 80
# overlap between viral and host CAP
BAM_REF_OVERLAP = 3

# maximum offset of match from start of reference sequence
BLAST_MAX_ALIGNMENT_START = 10
# minimum percentage identity of hit
BLAST_MIN_PERC_IDENTITY= 99
# number of nt the alignment can be shorter than the query sequence
BLAST_MAX_MISSING_NT = 5

# vsearch min length of clustered sequences
VSEARCH_MIN_LENGTH = '10'
# vsearch cluster_fast minimum sequence identity
VSEARCH_CLUSTER_IDENT = '0.9'

'''
parse a bam file and extract soft clipped reads at the beginning and end of each chromosome
'''

def parse_bam(sam_file, ref_seq, ref_length):
    
    clipped_reads_start = {}
    clip_mis_counter = {}
    
    clip_mis_list = []
    for read in sam_file.fetch(contig=ref_seq):
        # read must align to start or end of reference for regular capsnatch, most genes have mRNA on both ends
        if not read.is_unmapped:
            # alignment must be softclipped with the assumption that the clipped nucleotides belong to the host
            match = re.search("[0-9]+S$", read.cigarstring)
            if match:
                # get the sequence of the clipped part
                clip = int(match.group()[:-1]) + BAM_REF_OVERLAP
                # CAP seq from host is assumed to be 12-22nt, some margin is taken to account for long snatches
                if BAM_MIN_LENGTH_SOFTCLIP <= clip <= BAM_MAX_LENGTH_SOFTCLIP:
                        # first or second read of alignment
                        read12 = 1 if read.is_read1 == True else 2
                        
                        # add results to dictionary, key is ref_name, read_name, read12, value is ref_start, CIGAR, clip_length, clip_sequence, remaining_read_sequence)
                        clipped_reads_start[(read.reference_name,read.query_name, read12)] = (read.reference_start,read.cigarstring,clip,read.query_sequence[-clip:], read.query_sequence[0:read.query_length])
                else: 
                    if clip in clip_mis_counter:  clip_mis_counter[clip]+=1
                    else: clip_mis_counter[clip] =1
                    clip_mis_list.append(read.query_sequence[-clip:])
    return clipped_reads_start, clip_mis_counter, clip_mis_list

'''
deduplicate sequences and merge into clusters using vsearch
'''

def dedup_reads_vsearch(read_fasta, vsearch):
    
    
    p = subprocess.run([vsearch, '--output', '-', '--minseqlength', VSEARCH_MIN_LENGTH, '--fasta_width', '0', '--sizeout', '--derep_prefix', '-'],
                    input=read_fasta, encoding='ascii',stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p.stdout,p.stderr
    
'''
cluster deduplicated sequences propagating derep count
'''
def cluster_reads_vsearch(vsearch_fasta, vsearch):
    
    
    p = subprocess.run([vsearch, '--qmask', 'none', '--centroids', '-', '--id' ,VSEARCH_CLUSTER_IDENT, '--minseqlength' ,VSEARCH_MIN_LENGTH, '--sizeout', '--sizein' ,'--sizeorder','--fasta_width', '0', '--cluster_fast', '-'],
                    input=vsearch_fasta, encoding='ascii',stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p.stdout,p.stderr

'''
run blastn against the host genome to identify matches for the snatched sequences
Output format is a table with columns qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen qseq
'''

def blast_clusters(reads, blastdb, blastn):
    
    p = subprocess.run([blastn, '-task', 'blastn-short', '-query', '-', '-db', blastdb, '-outfmt', "6 std qlen qseq qframe sframe"],
                    input=reads, encoding='ascii',stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p.stdout

'''
Only keep snatched sequences where the blast hit 
starts at most BLAST_MAX_ALIGNMENT_START from the end of the query, 
has a minimum sequence identity of BLAST_MIN_PERC_IDENTITY 
and is at most BLAST_MAX_MISSING_NT shorter than the query sequence
''' 

def filter_blast(blast_out):
    
    blast_rows = iter(blast_out.splitlines())
    for row in blast_rows:
        #sys.stderr.write(str(row)) 
        row = row.strip().split('\t')
        #sys.stderr.write(str(row) + '\n')
        if len(row) <= 1: 
            log_out.write('empty\n')
            continue #empty row
        elif int(row[8]) > BLAST_MAX_ALIGNMENT_START: 
            log_out.write('s')
        #   continue # alignment must be at beginning of host cds
        elif float(row[2]) < BLAST_MIN_PERC_IDENTITY: 
            log_out.write('p')
            continue # alignment must be perfect 
        elif int(row[3]) < int(row[12]) - BLAST_MAX_MISSING_NT: 
            log_out.write('l')
            continue # whole query sequence must be aligned, allowing 1nt difference.
        else: log_out.write(str(row)+ '\n') 
        
def stack_bases(dedup_fasta):
    
    aligned = []
    for header,fseq in zip(*[iter(dedup_fasta.splitlines())]*2):
        
        dep = header.split(';')[1][5:]
        #sys.stderr.write(header.split(';')[1]+'\t'+ dep + '\n')
        if int(dep) >=5:
            fseq = fseq.rjust(100,'-')
            aligned.append('%s\n%s\n' %(header,fseq))
        #match = re.search("AG", fseq)
    return aligned

## https://github.com/lh3/readfq/blob/master/readfq.py
def readfq(fp): # this is a generator function
    last = None # this is a buffer keeping the last unprocessed line
    while True: # mimic closure; is it a bad idea?
        if not last: # the first record or a record following a fastq
            for l in fp: # search for the start of the next record
                if l[0] in '>@': # fasta/q header line
                    last = l[:-1] # save this line
                    break
        if not last: break
        name, seqs, last = last[1:].partition(" ")[0], [], None
        for l in fp: # read the sequence
            if l[0] in '@+>':
                last = l[:-1]
                break
            seqs.append(l[:-1])
        if not last or last[0] != '+': # this is a fasta record
            yield name, ''.join(seqs), None # yield a fasta record
            if not last: break
        else: # this is a fastq record
            seq, leng, seqs = ''.join(seqs), 0, []
            for l in fp: # read the quality
                seqs.append(l[:-1])
                leng += len(l) - 1
                if leng >= len(seq): # have read enough quality
                    last = None
                    yield name, seq, ''.join(seqs); # yield a fastq record
                    break
            if last: # reach EOF before reading enough quality
                yield name, seq, None # yield a fasta record instead
                break

        

def main():

    argparser = _prepare_argparser()
    args = argparser.parse_args()

    bam_file = args.bam_file
    read_file = args.read_file
    
    log_file = vars(args).get('log_file')
    if log_file == None:
        log_file = "/dev/stdout"

    
    out_prefix = args.out_file
    out_file = out_prefix + '.out'
    dedup_out_file = out_prefix + '.dedup.fa'
    cluster_out_file = out_prefix + '.cluster.fa'
    align_out_file = out_prefix + '.align.fa'
    fq_out_file = out_prefix + '.clipped.fq'
    clip_mis_out_file = out_prefix + '.clip_missed.text'
    
    blastn = vars(args).get('blastn')
    if blastn == None:
        blast=False
    else:
        blast_file = out_prefix + '.blast.out'
        blastn = args.blastn
        blast_db = args.blast_db
    vsearch = args.vsearch
    
    fastq = {}
    
    with open(read_file, 'r') as reads:
        n = 0
        for name, seq, qual in readfq(reads):
            n += 1
            
            name = name.split(' ')
            fastq[name[0]] = (seq, qual)
            
            
    with open(log_file, 'w') as log_out, open(fq_out_file, 'w') as fq_out,open(align_out_file, 'w') as align_out, open(clip_mis_out_file,'w') as cm_out, open(dedup_out_file, 'w') as dedup_out, open(cluster_out_file, 'w') as cluster_out:
        
                    
        with pysam.AlignmentFile(bam_file,'rb') as sam:
            for ref_seq,ref_length in zip(sam.references, sam.lengths):
                log_out.write("Running reference sequence " + ref_seq + "\n")
                clipped_reads_start, clip_mis_counter, clip_mis_list = parse_bam(sam, ref_seq, ref_length)
                cm_out.write('\n'.join(clip_mis_list))
                log_out.write('Number of reads with softclipped cap in ' + ref_seq + ':' + str(len(clipped_reads_start)) + '\n')
                for key, value in clip_mis_counter.items(): 
                    log_out.write(str(key) + ': ' +  str(value) + '\n')                          
                read_fasta = ''
                read_fastq = ''
                for keys,values in clipped_reads_start.items():
                    
                    _seq = Seq(values[3].rstrip('C')).reverse_complement()
                    _name = keys[1]
                    _clip = values[2] 
                    _fqqual = fastq[_name][1][-_clip:][::-1]
                    fq_out.write('@%s\n%s\n+\n%s\n' %(_name, _seq, _fqqual[:len(_seq)]))
                    # create fasta record, header is chrom_readName_read12_alignmentStart_start/end
                    read_fasta += '>%s_%i_start\n%s\n' %('_'.join(str(e) for e in keys),values[0],_seq)
                    # write all info into a table all key and value elements ase created by parse_bam
                    #table_out.write('%s\t%s\n' %('\t'.join(str(e) for e in keys), '\t'.join(str(e) for e in values)))
                
                
                # deduplicate all clipped reads at start using vsearch, 
                log_out.write("dedup\n")
                dedup_fasta, dedup_log = dedup_reads_vsearch(read_fasta, vsearch)
                # write deduplicated sequence to fasta
                log_out.write(dedup_log)
                dedup_out.write(dedup_fasta)
                
                log_out.write("align right\n")
                aligned_fasta = stack_bases(dedup_fasta)
                align_out.write(''.join(aligned_fasta))
                
                cluster_fasta,cluster_log = cluster_reads_vsearch(dedup_fasta, vsearch)# cluster the deduplicated sequences, the longest representative sequence is kept
                #sys.stderr.write(cluster_log)
                cluster_out.write(cluster_fasta)
                if blast:
                    # blast the deduplicated fasta files
                
                    blastresult = blast_clusters(dedup_fasta, blast_db, blastn)
                    with open(blast_file, 'a') as blast_out:
                        blast_out.write(blastresult)
                    filter_blast(blastresult)
                    # sys.stderr.write(blastresult)
                    
                    
                    

def _prepare_argparser(): 
    """Prepare optparser object. New arguments will be added in this
    function first.
    """
    description = """Identifies and annotates CAP sequences snatched by TSWV from host genome from alignment.
    Prepare the raw reads with the following steps:
    # Remove adapters and poor quality bases
    for i in *_1.fq.gz ;do echo "cutadapt --quality-base=64 -a AGATCGGAAGAGC -A AGATCGGAAGAGC -f fastq -m 50 -q 30 -o ${i/_1.fq.gz/_trimmed_1.fastq.gz} -p ${i/_1.fq.gz/_trimmed_2.fastq.gz} ${i} ${i/_1.fq.gz/_2.fq.gz}";done | parallel -j 6 :::
    # replace illigal phred character as vsearch doesn't quite like it
    for i in *trimmed_?.fastq.gz;do echo "gunzip -c ${i} | sed 's/j/i/g' | gzip -c > ${i/.fastq.gz/_fixed.fastq.gz}";done | parallel -j4 :::
    # merge overlapping reads and convert to ascii 64 phred
    for i in *trimmed_1_fixed.fastq.gz ;do echo "vsearch --fastq_mergepairs ${i} --reverse ${i/_1_fixed.fastq.gz/_2_fixed.fastq.gz} --fastqout ${i/_1_fixed.fastq.gz/_merged.fastq} --fastqout_notmerged_fwd ${i/_1_fixed.fastq.gz/_unmerged_fwd.fastq} --fastqout_notmerged_rev ${i/_1_fixed.fastq.gz/_unmerged_rev.fastq} --fastq_ascii 64 --threads 2";done | parallel -j 6 ::: 
    # map both read sets
    for i in *_merged.fastq;do echo "bwa mem -t 2 -M harm_test/TSWV-lettuce.fasta $i | samtools sort -@2 -O bam -T ${i}_tmp -o ${i/.fastq/_sorted.bam} -"; done | parallel -j3 :::
    for i in *_fwd.fastq;do echo "bwa mem -t 2 -M harm_test/TSWV-lettuce.fasta $i ${i/_fwd.fastq/_rev.fastq} | samtools sort -@2 -O bam -T ${i}_tmp -o ${i/_fwd.fastq/_sorted.bam} -"; done | parallel -j6 :::
    # index bam files
    for i in *trimmed*bam; do echo "samtools index ${i}";done | parallel -j 6 :::
    # only retain mapped reads
    for i in *trimmed*sorted.bam; do echo "samtools view ${i} -F 4 -b -o ${i/.bam/mapped.bam}";done | parallel -j 6 :::
    # index filtered bam files
    for i in *sortedmapped.bam; do samtools index $i;done
    # merge bamfiles from overlapping and non-overlapping read alignments
    for i in *_unmerged_sortedmapped.bam; do echo "samtools merge -O bam ${i%%_*}_trimmed_sorted_mapped_both.bam ${i} ${i/unmerged/merged}";done | parallel -j6 :::
    # index final bam
    for i in *_trimmed_sorted_mapped_both.bam; do echo "samtools index ${i}";done | parallel -j6 :::
    # use these bam files as input for capsnatch.py"""

    argparser = ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)

    argparser.add_argument("-b","--bam_file",dest="bam_file",type=str, required=True,
                         help="bam file with reads mapped to reference with local alignemnt (bowtie2)")
    argparser.add_argument("-o", "--out_file_prefix", dest="out_file", type=str, required=True,
                           help="output file destination prefix")
    argparser.add_argument("-v", "--vsearch", dest="vsearch", type=str, required=True,
                           help="path to vsearch executable")
    argparser.add_argument("-r", "--reads", dest="read_file", type=str, required=True,
                           help="fastq readfile")
    argparser.add_argument("-n", "--blastn", dest="blastn", type=str, required=False,
                           help="path to blastn executable")
    argparser.add_argument("-d", "--blast_db", dest="blast_db", type=str, required=False,
                           help="name of local blastdb")
    argparser.add_argument("-l", "--log", dest="log_file", type=str, help="log file, if not given, stderr")
    return argparser

if __name__ == "__main__":
    main()
